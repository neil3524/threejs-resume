import Navbar from "./Navbar";
import Intro from "./Resume/Intro";
import Projects from "./Resume/Projects";
import Experience from "./Resume/Experience";
import Education from "./Resume/Education";
import Awards from "./Resume/Awards";

function App() {
  return (
    <>
      <Navbar />
      <div id="wrapper">
        <Intro />
        <Projects />
        <Experience />
        <Education />
        <Awards />
        <div id="tag">
          <p>Made with 💖 by Neil Fisher</p>
          <p>
            Email me: <a href="mailto:neil@neilf.dev">neil@neilf.dev</a>
          </p>
        </div>
      </div>
    </>
  );
}

export default App;
