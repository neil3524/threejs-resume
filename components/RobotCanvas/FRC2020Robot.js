import React, { useMemo } from "react";
import { useGLTF } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import * as THREE from "three";

const gltfURL = "./models/FRC2020_Robot_Goal_Animation_Meshopt.gltf";
const animationFrames = 200,
  animationFps = 60,
  animationDuration = animationFrames / animationFps;

useGLTF.preload(gltfURL);

function FRC2020Robot() {
  const gltf = useGLTF(gltfURL);
  //   console.log("gltf", gltf);

  /* ball models */
  const yellow = useMemo(() => new THREE.Color(0xffff00), []);
  const darkBlue = useMemo(() => new THREE.Color("#0023a8"), []);
  // ball 1
  gltf.scene.children[1].children[0].material.flatShading = false;
  gltf.scene.children[1].children[0].material.emissive = yellow;
  gltf.scene.children[1].children[0].geometry.computeVertexNormals();
  // ball 2 -> copy ball 1 material and geometry
  gltf.scene.children[2].children[0].material = gltf.scene.children[1].children[0].material;
  gltf.scene.children[2].children[0].geometry = gltf.scene.children[1].children[0].geometry;

  /* robot model */
  // const red = useMemo(() => new THREE.Color(0xff0000), []);
  // gltf.scene.children[0].children[0].children[26].material.emissive = red;
  gltf.scene.children[0].children[0].children[26].material.metalness = 0.4;

  /* floor */
  gltf.scene.children[4].material.color = darkBlue;

  /* robot and ball animations */
  const mixer = useMemo(() => new THREE.AnimationMixer(gltf.scene), [gltf.scene]);
  gltf.animations.forEach((animation, idx) => {
    // make sure the animations are synced
    animation.duration = animationDuration;
    mixer.clipAction(animation).play();
  });

  useFrame((state, delta) => {
    mixer.update(delta);
  });

  return gltf ? <primitive object={gltf.scene} /> : null;
}

export default React.memo(FRC2020Robot);
