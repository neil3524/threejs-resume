import { Suspense } from "react";
import RobotCanvas from "./RobotCanvas";
import LoadingCanvas from "./LoadingCanvas";

export default function RobotCanvasSuspense() {
  return (
    <Suspense fallback={<LoadingCanvas />}>
      <RobotCanvas />
    </Suspense>
  );
}
