import React, { useEffect, useMemo, useRef } from "react";
import * as THREE from "three";
import { useThree, useFrame } from "@react-three/fiber";
import FRC2020Robot from "./FRC2020Robot";
import { Sky, Stars, Cloud, OrbitControls, Sphere } from "@react-three/drei";
// import { useTexture, useHelper } from "@react-three/drei";
import Effects from "./SceneEffects";

/* function Environment() {
  const { scene } = useThree();
  const texture = useTexture("./images/space5.jpg");
  texture.mapping = THREE.EquirectangularReflectionMapping;
  scene.environment = texture;
  scene.background = texture;
  return null;
} */

function signOf(x) {
  return x < 0 ? -1 : 1;
}

let orbitControlsMoving = false;
let debounceFlag = false;
// determined at 144fps... scale for 60fps
let autoRotateSpeedMin = 0.15,
  autoRotateSpeedMax = 0.55;
const defaultMinAzimuthAngle = -0.76,
  defaultMaxAzimuthAngle = 2;
const doublePI = Math.PI * 2;
const amplitude = -autoRotateSpeedMax / 2;
let sign = signOf(autoRotateSpeedMin);

function DollyCam({ orbitControlsRef }) {
  useEffect(() => {
    orbitControlsRef.current.autoRotate = true;
    orbitControlsRef.current.autoRotateSpeed = autoRotateSpeedMin;
    orbitControlsRef.current.minAzimuthAngle = defaultMinAzimuthAngle;
    orbitControlsRef.current.maxAzimuthAngle = defaultMaxAzimuthAngle;
  }, [orbitControlsRef]);

  // let autoRotateSpeedSet = false;
  // const commonFPSValues = [60, 120, 144, 240, 360, 480];
  useFrame((state, delta) => {
    // scale the speed based on the framerate (speeds were measured at 144fps)
    /* if (!autoRotateSpeedSet) {
      setTimeout(() => {
        // get fps
        const fps = 1 / state.clock.getDelta();
        const differences = commonFPSValues.map((value) => Math.abs(value - fps));
        console.log(fps, differences);
        const idxClosestToZero = differences.indexOf(Math.min(...differences));
        const roundedFps = commonFPSValues[idxClosestToZero];
        const scale = 144 / roundedFps;
        autoRotateSpeedMin = autoRotateSpeedMin * scale;
        autoRotateSpeedMax = autoRotateSpeedMax * scale;
      }, 2500);
      autoRotateSpeedSet = true;
    } */

    if (!orbitControlsMoving && !debounceFlag) {
      const azimuthalAngle = orbitControlsRef.current.getAzimuthalAngle();
      const minAzimuthAngle = orbitControlsRef.current.minAzimuthAngle;
      const maxAzimuthAngle = orbitControlsRef.current.maxAzimuthAngle;

      // const azimuthalAngleRatio = (azimuthalAngle - minAzimuthAngle) / (maxAzimuthAngle - minAzimuthAngle);
      const azimuthalAngleRatio = THREE.MathUtils.inverseLerp(minAzimuthAngle, maxAzimuthAngle, azimuthalAngle);
      // s = ((a - 0.5) * 1.65)^2 + 0.35
      // orbitControlsRef.current.autoRotateSpeed = (Math.pow((azimuthalAngleRatio - 0.5) * 1.65, 2) + autoRotateSpeedMin) * sign;
      // s = -a * cos(2 * pi * x) - a + 0.3 * sign(y)
      orbitControlsRef.current.autoRotateSpeed = (-amplitude * Math.cos(doublePI * azimuthalAngleRatio) - amplitude + autoRotateSpeedMin) * sign;
      if (azimuthalAngle === minAzimuthAngle || azimuthalAngle === maxAzimuthAngle) {
        orbitControlsRef.current.autoRotateSpeed = -orbitControlsRef.current.autoRotateSpeed;
        sign = signOf(orbitControlsRef.current.autoRotateSpeed);

        debounceFlag = true;
        setTimeout(() => {
          debounceFlag = false;
        }, 50);
      }
    }
  });
  return null;
}

/**
 *
 * @param {Number} aspect aspect ratio of the window
 * @param {Number} hFov horizontal field of view in degrees
 * @returns vertical fov in degrees
 */
export function getCameraHFovFromVFov(aspect, hFov = 90) {
  // calculate vertical fov based on aspect ratio and horizontal fov
  return (Math.atan(Math.tan(((hFov / 2) * Math.PI) / 180) / aspect) * 2 * 180) / Math.PI; // degrees
}

function Sun() {
  return <Sky distance={4000} inclination={0.494} turbidity={8} rayleigh={2.5} azimuth={60 / 360} mieCoefficient={0.005} mieDirectionalG={0.65} />;
}

function Moon() {
  return <Sky distance={4000} inclination={0.52} turbidity={0} rayleigh={0.02} azimuth={60 / 360} mieCoefficient={0} mieDirectionalG={0} />;
}

function Scene({ orbitControlsRef, minDistance, maxDistance, renderAdaptive, daytime }) {
  const { gl, camera, scene, clock } = useThree();
  // set clock autostart to true so it starts again when the frameloop is changed
  // this is needed for useFrame delta to not be 0 when the clock is not running automatically
  clock.autoStart = true;
  // lowering exposure to make the sky darker
  gl.toneMappingExposure = 0.6;

  /*
  // set renderAdaptive depending on fps
  let counter = 0;
  const [renderAdaptive, setRenderAdaptive] = useState(gpuTier.tier !== 3 || gpuTier.isMobile);
  useFrame((state, delta) => {
    if (state.frameloop !== "always" || renderAdaptive) return;

    // calculate fps using delta time (1s/16.66ms = 60fps)
    const fps = 1 / delta;
    if (fps < 56 || (fps > 120 && fps < 136)) {
      counter++;
      console.log(counter);
      if (counter > 10) setRenderAdaptive(true);
    }
  });
  */

  // initialize orbit controls
  useEffect(() => {
    const canvasContainer = document.querySelector("#canvas-container");

    const keydown = (e) => {
      // if shift is held down, enable orbit controls zoom
      if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        // enable zoom
        orbitControlsRef.current.enableZoom = true;
      }
    };
    document.addEventListener("keydown", keydown);

    const keyup = (e) => {
      if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        // disable zoom if not fullscreen
        if (!canvasContainer.hasAttribute("fullscreen")) {
          orbitControlsRef.current.enableZoom = false;
        }
      }
    };
    document.addEventListener("keyup", keyup);

    const fullscreenChangeHandler = () => {
      // on document exit fullscreen, disable orbit controls
      if (document.fullscreenElement) {
        if (canvasContainer.hasAttribute("fullscreen")) {
          orbitControlsRef.current.enableZoom = true;
        }
      } else {
        canvasContainer.removeAttribute("fullscreen");
        orbitControlsRef.current.enableZoom = false;
      }
    };
    document.addEventListener("fullscreenchange", fullscreenChangeHandler);

    return () => {
      document.removeEventListener("keydown", keydown);
      document.removeEventListener("keyup", keyup);
      document.removeEventListener("fullscreenchange", fullscreenChangeHandler);
    };
  }, [orbitControlsRef]);

  const spotLightRef = useRef();

  const lightRed = useMemo(() => new THREE.Color("#ff7a7a"), []);
  const lightBlue = useMemo(() => new THREE.Color("#5e5eff"), []);
  const red = useMemo(() => new THREE.Color("#ff0000"), []);
  const blue = useMemo(() => new THREE.Color("#0000ff"), []);
  const darkRed = useMemo(() => new THREE.Color("#420000"), []);
  const darkBlue = useMemo(() => new THREE.Color("#002099"), []);

  let goalMaterial = useRef();
  let robotBumperMaterial = useRef();
  let floorMaterial = useRef();
  useEffect(() => {
    goalMaterial.current = scene.children[4].children[3].children[12].material;
    robotBumperMaterial.current = scene.children[4].children[0].children[0].children[26].material;
    floorMaterial.current = scene.children[4].children[4].material;
  }, [scene.children]);

  if (goalMaterial.current && robotBumperMaterial.current && floorMaterial.current) {
    // change model color depending on daytime
    if (!daytime) {
      // goal
      goalMaterial.current.color = red;
      goalMaterial.current.metalness = 0;
      // robot bumper
      robotBumperMaterial.current.color = red;
      robotBumperMaterial.current.emissive = red;
      // floor
      floorMaterial.current.color = darkBlue;
      // spotlight
      spotLightRef.current.color = lightBlue;
    } else {
      // goal
      goalMaterial.current.color = blue;
      goalMaterial.current.metalness = 0.35;
      // robot bumper
      robotBumperMaterial.current.color = blue;
      robotBumperMaterial.current.emissive = blue;
      // floor
      floorMaterial.current.color = darkRed;
      // spotlight
      spotLightRef.current.color = lightRed;
    }
  }

  const spotLightRef2 = useRef();
  // useHelper(spotLightRef2, THREE.SpotLightHelper);
  // set spotlight position
  useEffect(() => {
    spotLightRef2.current.target.position.set(0, 1.8, -1.5);
    spotLightRef2.current.target.updateMatrixWorld();
  }, []);

  const sunRef = useRef();

  return (
    <>
      {/* Lighting */}
      <ambientLight intensity={0.6} />
      <spotLight ref={spotLightRef} position={[10, 10, 10]} angle={0.15} intensity={4} penumbra={1} color={"#0090ff"} />
      <spotLight ref={spotLightRef2} position={[-2, 10, 20]} angle={0.065} intensity={2} penumbra={1} color={"#ffffff"} />
      <pointLight position={[-10, -10, -10]} />

      {/* Models */}
      <FRC2020Robot />
      {!renderAdaptive && (
        <Sphere ref={sunRef} position={daytime ? [-1140, -42, -1978] : [-1140, 144, -1978]} scale={new THREE.Vector3(23, 23, 23)}>
          <meshBasicMaterial attach="material" color={daytime ? "#feb646" : "#82a3ff"} />
        </Sphere>
      )}

      {/* Environment */}
      {/* <Suspense fallback={null}>
        <Environment />
      </Suspense> */}
      {daytime ? <Sun /> : <Moon />}
      <Stars radius={400} depth={100} count={5000} factor={30} saturation={0.5} fade />
      {/* {!renderAdaptive && <Stars radius={400} depth={100} count={5000} factor={30} saturation={0.5} fade />} */}
      {!renderAdaptive && <Cloud opacity={0.05} width={16} depth={2} />}

      {/* Effects */}
      <Effects sunRef={sunRef} renderAdaptive={renderAdaptive} daytime={daytime} />

      {/* Controls */}
      <DollyCam orbitControlsRef={orbitControlsRef} />
      <OrbitControls
        ref={orbitControlsRef}
        enableZoom={false}
        target={[0, 1.8, -1.5]}
        minDistance={minDistance}
        maxDistance={maxDistance}
        onStart={() => {
          orbitControlsMoving = true;
          orbitControlsRef.current.minAzimuthAngle = -Infinity;
          orbitControlsRef.current.maxAzimuthAngle = Infinity;
        }}
        onChange={() => {
          // make some parts invisible depending on the distance of the camera
          const distance = camera.position.distanceTo(orbitControlsRef.current.target);
          const visible = distance < 12;
          // motors
          scene.children[4].children[0].children[0].children[7].visible = visible;
        }}
        onEnd={() => {
          orbitControlsMoving = false;
          orbitControlsRef.current.minAzimuthAngle = defaultMinAzimuthAngle;
          orbitControlsRef.current.maxAzimuthAngle = defaultMaxAzimuthAngle;
        }}
      />
    </>
  );
}

export default React.memo(Scene);
