import React from "react";

function LargeScreenButton({ debounceRef, defaultCanvasSize, largeCanvasSize, canvasSize, setCanvasSize }) {
  return (
    <button
      id="large-screen-button"
      onClick={() => {
        const canvasContainer = document.querySelector("#canvas-container");
        if (!canvasContainer.hasAttribute("large-screen")) {
          canvasContainer.setAttribute("large-screen", "");

          // set canvas height to full screen
          setCanvasSize(largeCanvasSize);

          // debounceRef.current = 10;
          setTimeout(() => {
            canvasContainer.scrollIntoView({ behavior: "smooth", block: "start" });
            // debounceRef.current = 100;
          }, 50);
        } else {
          canvasContainer.removeAttribute("large-screen");

          // set canvas height to default
          setCanvasSize(defaultCanvasSize);

          // debounceRef.current = 10;
          setTimeout(() => {
            canvasContainer.scrollIntoView({ behavior: "smooth", block: "center" });
            // debounceRef.current = 100;
          }, 50);
        }
      }}
    >
      <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#ffffff">
        <rect fill="none" height="24" width="24" />
        <polygon points="21,11 21,3 13,3 16.29,6.29 6.29,16.29 3,13 3,21 11,21 7.71,17.71 17.71,7.71" />
      </svg>
    </button>
  );
}

export default React.memo(LargeScreenButton);
