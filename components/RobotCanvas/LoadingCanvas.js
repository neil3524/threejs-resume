import { Loader } from "@react-three/drei";

function LoadingCanvas() {
  return (
    <Loader
      containerStyles={{
        position: "relative",
        height: "40rem",
      }}
    />
  );
}

export default LoadingCanvas;
