import { useState, useEffect, useRef, useMemo } from "react";
import * as THREE from "three";
import { Canvas } from "@react-three/fiber";
import Scene from "./Scene";
import { useDetectGPU } from "@react-three/drei";
import { useInView } from "react-intersection-observer";
import FullscreenButton from "./FullscreenButton";
import CanvasCheckbox from "./CanvasCheckbox";
// import LargeScreenButton from "./LargeScreenButton";
// import { Perf } from "r3f-perf";
// import "../../styles/CanvasControls.css";

import isTouchScreen from "../IsTouchScreen";

function RobotCanvas() {
  const gpuTier = useDetectGPU();
  // const clientFPS = useMemo(() => getFPS(), []);
  // const renderAdaptive = gpuTier.tier !== 3 || gpuTier.isMobile;
  const [renderAdaptive, setRenderAdaptive] = useState(gpuTier.tier !== 3 || gpuTier.isMobile);

  const defaultCanvasSize = { width: "100%", height: "40rem" },
    // largeCanvasSize = { width: defaultCanvasSize.width, height: "100vh" },
    fullCanvasSize = { width: "100vw", height: "100vh" };
  const [canvasSize, setCanvasSize] = useState(defaultCanvasSize);

  const [inViewRef, inView] = useInView({
    initialInView: true,
    delay: 100,
    rootMargin: "0px 0px 200px 0px",
  });

  const orbitControlsRef = useRef();
  const rangeRef = useRef();

  // const aspect = window.innerWidth / (parseFloat(canvasHeight) * 16);
  // let aspect = window.innerWidth / (parseFloat(canvasHeight) * 16);
  // useEffect(() => {
  //   // calculate vertical fov based on aspect ratio of the canvas
  //   aspect = window.innerWidth / (parseFloat(canvasHeight) * 16);
  //   console.log("rerender robot canvas with aspect: " + aspect);
  //   // vFov.current = getCameraVFovFromHFov(aspect);
  // });

  const perfModeRef = useRef();

  const orbitControlsTarget = useMemo(() => new THREE.Vector3(0, 1.8, -1.5), []);
  const minDistance = 5,
    maxDistance = 35;
  useEffect(() => {
    rangeRef.current.min = minDistance;
    rangeRef.current.max = maxDistance;

    // calculate steps based on distances
    // 5 - 35 / 1000
    const stepsWanted = 1000;
    rangeRef.current.step = (rangeRef.current.max - rangeRef.current.min) / stepsWanted;

    /* disable touches on mobile */
    if (isTouchScreen()) {
      const canvasElem = document.querySelector("#canvas-container canvas");
      canvasElem.style.pointerEvents = "none";
    }

    // check the checkbox accordingly
    perfModeRef.current.checked = renderAdaptive;
  }, [renderAdaptive]);

  const [daytime, setDaytime] = useState(false);

  // load for one second, then set to false to make scrolling past canvas smoother
  const [load, setLoad] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setLoad(false);
    }, 1000);
  }, [load]);

  return (
    <>
      <Canvas
        ref={inViewRef}
        resize={{ debounce: 50 }}
        mode="concurrent"
        linear
        style={{
          width: canvasSize.width,
          height: canvasSize.height,
        }}
        frameloop={inView || load ? "always" : "never"}
        camera={{
          fov: 90,
          zoom: 5.5,
          position: [11, 1.5, 10],
          far: 3000,
        }}
      >
        {/* <Perf /> */}
        <Scene daytime={daytime} renderAdaptive={renderAdaptive} orbitControlsRef={orbitControlsRef} minDistance={minDistance} maxDistance={maxDistance} />
      </Canvas>
      <div className="top-right-settings">
        <CanvasCheckbox label="Daytime" setState={setDaytime} />
        <CanvasCheckbox labelRef={perfModeRef} label="Performance mode" setState={setRenderAdaptive} />
        {/* <LargeScreenButton defaultCanvasSize={defaultCanvasSize} largeCanvasSize={largeCanvasSize} canvasSize={canvasSize} setCanvasSize={setCanvasSize} /> */}
        <FullscreenButton
          orbitControlsRef={orbitControlsRef}
          defaultCanvasSize={defaultCanvasSize}
          fullCanvasSize={fullCanvasSize}
          canvasSize={canvasSize}
          setCanvasSize={setCanvasSize}
        />
      </div>
      <input
        ref={rangeRef}
        id="zoom-slider"
        type="range"
        min={0}
        max={30}
        step={0.1}
        onChange={(e) => {
          const value = parseFloat(e.target.value);
          orbitControlsRef.current.minDistance = orbitControlsRef.current.maxDistance = value;
          // move & update orbitControls now
          orbitControlsRef.current.update();

          // 0 to 1 range
          const valueRatio = (value - minDistance) / (maxDistance - minDistance);
          orbitControlsRef.current.target.x = THREE.MathUtils.lerp(0, orbitControlsTarget.x, valueRatio);
          orbitControlsRef.current.target.y = THREE.MathUtils.lerp(0.5, orbitControlsTarget.y, valueRatio);
          orbitControlsRef.current.target.z = THREE.MathUtils.lerp(-0.4, orbitControlsTarget.z, valueRatio);

          // reset the range to the regular values so user can still manually zoom using shift + scroll
          orbitControlsRef.current.minDistance = minDistance;
          orbitControlsRef.current.maxDistance = maxDistance;
        }}
      />
    </>
  );
}

export default RobotCanvas;
