/* eslint-disable react-hooks/rules-of-hooks */
import * as THREE from "three";
import React, { useRef, useEffect, useState } from "react";
import { useFrame, useThree } from "@react-three/fiber";
import { EffectComposer, Noise, Vignette, ChromaticAberration, GodRays, Outline, BrightnessContrast } from "@react-three/postprocessing";

let chromaticAbAnimationFin = false;
let allowAnimation = false;

/**
 * this needs to be its own component because it needs to have its own state.
 * the ChromaticAberration effect needs to be able to change the offset without re-rendering the whole scene.
 * @param props {import("react").PropsWithChildren<{}>} - props with renderAdaptive boolean for performance
 * @returns {React.ReactElement} Effects
 */
function Effects({ sunRef, renderAdaptive, daytime }) {
  if (renderAdaptive) {
    return (
      <EffectComposer>
        <BrightnessContrast contrast={daytime ? 0.25 : 0.35} brightness={0.1} />
        <Vignette eskil={false} offset={0.01} darkness={0.2} />
        <Noise opacity={0.022} />
      </EffectComposer>
    );
  }

  const { scene } = useThree();

  let timeout;
  const chromaticAbRef = useRef();
  /**
   * ChromaticAberration offset animation
   */
  useFrame((state, delta) => {
    if (!timeout) {
      timeout = setTimeout(() => {
        allowAnimation = true;
      }, 500);
    }
    if (allowAnimation && chromaticAbRef.current && !chromaticAbAnimationFin) {
      // smooth transition from default offset to to 0 using damp
      let x = THREE.MathUtils.damp(chromaticAbRef.current.offset.x, 0, 1.7, delta);
      let y = THREE.MathUtils.damp(chromaticAbRef.current.offset.y, 0, 1.7, delta);
      // round to 0 if close enough
      x = x > 0.001 ? x : 0;
      y = y > 0.001 ? y : 0;

      chromaticAbRef.current.offset.set(x, y);

      if (x === 0 && y === 0) chromaticAbAnimationFin = true;
    }
  }, -10);

  const [state, setState] = useState({});
  // when scene is fully loaded set the ball refs for GodRays effect
  useEffect(() => {
    const model = scene.children[4];
    // floor = 4, ball = 1, 2
    setState({
      ballRefs: [{ current: model.children[1].children[0] }, { current: model.children[2].children[0] }],
      goalRef: { current: model.children[3].children[12] },
    });
  }, [scene.children]);

  return (
    <EffectComposer autoClear={false} multisampling={0}>
      {!chromaticAbAnimationFin && <ChromaticAberration ref={chromaticAbRef} offset={[0.02, 0.015]} />}
      <BrightnessContrast contrast={daytime ? 0.25 : 0.35} brightness={0.1} />
      <Vignette eskil={false} offset={0.01} darkness={0.2} />
      <Noise opacity={0.022} />
      {state.goalRef && (
        <Outline
          key={"goal"}
          selection={state.goalRef}
          selectionLayer={1}
          visibleEdgeColor={daytime ? "orange" : "cyan"}
          hiddenEdgeColor={daytime ? "orange" : "cyan"}
          blur
          edgeStrength={100}
          width={2000}
        />
      )}
      {state.ballRefs && (
        <Outline
          key={"balls"}
          selection={state.ballRefs}
          selectionLayer={2}
          visibleEdgeColor="#ffff05"
          hiddenEdgeColor="#ffff05"
          blur
          edgeStrength={100}
          width={1500}
        />
      )}
      {state.ballRefs &&
        state.ballRefs.map((mesh, idx) => (
          <GodRays key={idx} sun={mesh.current} samples={15} density={0.97} decay={0.94} weight={0.6} exposure={0.12} clampMax={1} blur={true} />
        ))}
      {sunRef.current && <GodRays sun={sunRef.current} samples={18} density={0.96} decay={0.94} weight={0.2} exposure={0.72} clampMax={0.8} blur={true} />}
    </EffectComposer>
  );
}

export default React.memo(Effects);
