export default function CanvasCheckbox({ labelRef, label, setState }) {
  return (
    <div className="canvas-setting">
      <label>
        {label}
        <input
          ref={labelRef}
          type="checkbox"
          onChange={(evt) => {
            setState(evt.target.checked);
          }}
        />
      </label>
    </div>
  );
}
