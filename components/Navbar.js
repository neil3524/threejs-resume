import React, { useState, useRef, useEffect } from "react";
import { styled, useTheme } from "@mui/material/styles";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import HomeRounded from "@mui/icons-material/HomeRounded";
import Trophy from "@mui/icons-material/EmojiEventsRounded";
import School from "@mui/icons-material/SchoolRounded";
import RobotArm from "@mui/icons-material/PrecisionManufacturingRounded";
import Experience from "@mui/icons-material/ConnectWithoutContactRounded";
import Download from "@mui/icons-material/DownloadRounded";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import ContentCopy from "@mui/icons-material/ContentCopyRounded";
import Tooltip from "@mui/material/Tooltip";

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#1a1c2e",
    },
    background: {
      main: "#0d0e17",
      paper: "#1a1c2e7e",
    },
    text: {
      primary: "#ffffff",
      secondary: "#e0e0e0",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 750,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
});

const drawerWidth = 200;
const closedDrawerWidth = 65;
const closedDrawerWidthSM = 65 - 10;

const openedMixin = (theme) => ({
  width: drawerWidth,
  background: "#1a1c2e",
  transition: [
    theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    theme.transitions.create("background", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  ],
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: [
    theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    theme.transitions.create("background", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  ],
  overflowX: "hidden",
  // width: `calc(${theme.spacing(7)} - 1px)`,
  width: 0,
  [theme.breakpoints.up("sm")]: {
    // width: `calc(${theme.spacing(9)} - 1px)`,
    width: closedDrawerWidth,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

function NavEmail() {
  const emailRef = useRef();
  const defaultText = "Copy Email";
  const [tooltipText, setTooltipText] = useState(defaultText);

  return (
    <div id="nav-email">
      <Tooltip title={tooltipText} placement="right" onMouseOut={() => setTooltipText(defaultText)}>
        <div
          onClick={(evt) => {
            const email = emailRef.current.textContent;
            navigator.clipboard.writeText(email).then(
              () => {
                setTooltipText("Copied to clipboard!");
              },
              () => {
                setTooltipText("Copied to clipboard failed");
              }
            );
          }}
        >
          <p ref={emailRef}>neil@neilf.dev</p>
          <ContentCopy id="copy-email" />
        </div>
      </Tooltip>
    </div>
  );
}

function NavBar() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const iconSize = "28px";
  const iconStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "white",
    fontSize: iconSize,
    width: closedDrawerWidthSM,
    height: "50px",
    [theme.breakpoints.up("sm")]: {
      width: closedDrawerWidth,
    },
  };

  const closeBtnRef = useRef();

  const clearCanvasBtnRef = useRef();
  useEffect(() => {
    // if scrolled to the top, add fade class to button
    const wrapper = document.querySelector("#wrapper");
    if (clearCanvasBtnRef.current) {
      const handleScroll = () => {
        if (wrapper.scrollTop > 150) {
          clearCanvasBtnRef.current.classList.add("fade");
        } else {
          clearCanvasBtnRef.current.classList.remove("fade");
        }
      };
      wrapper.addEventListener("scroll", handleScroll);
    }
  }, []);

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              color: "white",
              marginLeft: "-9px",
              marginRight: "clamp(2px, 26px, 1.8vw)",
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
            Neil&apos;s Portfolio
          </Typography>
          <NavEmail />
          <button
            ref={clearCanvasBtnRef}
            id="clear-canvas"
            className="header-btn"
            onClick={() => {
              const canvas = document.querySelector("#paint-canvas");
              const ctx = canvas.getContext("2d");
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            }}
          >
            Clear Canvas
          </button>
          <Tooltip title="Download as PDF" placement="bottom">
            <a className="header-btn" href="./Resume/Neil_Fisher_Resume.pdf" download="Neil_Fisher_Resume.pdf">
              Resume <Download sx={{ marginLeft: "2px" }} />
            </a>
          </Tooltip>
        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={open}>
        <DrawerHeader>
          <IconButton
            ref={closeBtnRef}
            sx={{
              color: "white",
            }}
            onClick={handleDrawerClose}
          >
            {theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            height: "100%",
            justifyContent: "center",
          }}
        >
          <List>
            <ListItem
              button
              sx={{
                padding: "0px",
              }}
            >
              <ListItemIcon
                sx={iconStyle}
                onClick={() => {
                  document.querySelector(`#intro`).scrollIntoView({ behavior: "smooth", block: "start" });
                  if (window.innerWidth < 750) {
                    setTimeout(() => {
                      closeBtnRef.current.click();
                    }, 500);
                  }
                }}
              >
                <HomeRounded fontSize="inherit" />
              </ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
          </List>
          <Divider />
          <List>
            {["Projects", "Experience", "Education", "Awards"].map((text, index) => (
              <ListItem
                button
                key={text}
                onClick={() => {
                  const block = document.querySelector(`#${text}`);
                  // block.scrollIntoView({ behavior: "smooth", block: "start" });
                  // scrollTo block minus 50px
                  const wrapper = document.querySelector("#wrapper");
                  wrapper.scrollTo({
                    top: block.offsetTop - 80,
                    behavior: "smooth",
                  });
                  if (window.innerWidth < 750) {
                    setTimeout(() => {
                      closeBtnRef.current.click();
                    }, 500);
                  }
                }}
                sx={{
                  padding: "0px",
                }}
              >
                <ListItemIcon sx={iconStyle}>
                  {text === "Projects" ? (
                    <RobotArm fontSize="inherit" />
                  ) : text === "Education" ? (
                    <School fontSize="inherit" />
                  ) : text === "Experience" ? (
                    <Experience fontSize="inherit" />
                  ) : (
                    <Trophy fontSize="inherit" />
                  )}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      </Drawer>
    </ThemeProvider>
  );
}

export default NavBar;
