import { Component, createElement } from "react";

class Block extends Component {
  render() {
    return (
      <section className="block">
        {this.props.header && createElement(`h${this.props.header.level || 2}`, this.props.header.props, this.props.header.text)}
        {this.props.description && <p className="description">{this.props.description}</p>}
        {this.props.children}
      </section>
    );
  }
}

export default Block;
