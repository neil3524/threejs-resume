export default function isTouchScreen() {
  return "ontouchstart" in window || navigator.maxTouchPoints;
}
