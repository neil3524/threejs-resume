/* eslint-disable @next/next/no-img-element */
import SectionTitle from "./SectionTitle";

function ExpCard({ date, title, company, descriptionPts, img, justifyLeft, justifyRight }) {
  const justifyLeftStyle = justifyLeft ? " justify-left" : justifyRight ? " justify-right" : "";
  return (
    <div className={"item-container" + justifyLeftStyle}>
      {!justifyRight && img && img.url && <img className="item-media" src={img.url} alt={img.alt || ""} width={250} height={250} />}
      <span>
        <p className="item-date">
          <i>{date}</i>
        </p>
        <p className="item-title">
          <b>{title}</b>
        </p>
        <p>{company}</p>
        <ul>{descriptionPts}</ul>
      </span>
      {justifyRight && img && img.url && <img className="item-media" src={img.url} alt={img.alt || ""} width={250} height={250} />}
    </div>
  );
}

export default function Experience() {
  return (
    <section id="Experience" className="block">
      <SectionTitle title="Experience" />

      <ExpCard
        date={"2019-06 to 2019-08"}
        title={"Hardware Lab Assistant"}
        img={{ url: "images/DRW.jpg", alt: "DRW" }}
        justifyLeft
        company={
          <>
            DRW - <i>Trading company, Montreal, Quebec</i>
          </>
        }
        descriptionPts={
          <>
            <li>Designed and 3d printed enclosure for PCB used by traders</li>
            <li>
              IT work - <br />
              Assembled desktops and laptops, Installed custom versions of Windows &amp; Linux
            </li>
          </>
        }
      />

      <ExpCard
        date={"2021-01 to 2021-06"}
        title={"Computer Science Tutor"}
        img={{ url: "images/dawson_tutoring.png", alt: "Dawson" }}
        justifyRight
        company={
          <>
            Dawson College, <i>Westmount</i>
          </>
        }
        descriptionPts={
          <>
            <li>Tutored average and advanced students in computer science</li>
            <li>Courses tutored: Web Development, Programming, Linux, Database</li>
          </>
        }
      />

      <ExpCard
        date={"2016 to Present"}
        title={"Robotics Mentor"}
        img={{ url: "images/sonic_howl.png", alt: "Sonic Howl" }}
        justifyLeft
        company={
          <>
            Sonic Howl - <i>FIRST Robotics Competition (FRC) Team, Montreal, Quebec</i>
          </>
        }
        descriptionPts={
          <>
            <li>
              Montreal 2019 Competition Winners ➔ Detroit Championship
              <ul>
                <li>Lead Programmer / CAD Designer / Driver</li>
              </ul>
            </li>
            <li>Mentoring students since 2019 high school graduation</li>
          </>
        }
      />
    </section>
  );
}
