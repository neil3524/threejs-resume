import { useEffect, useCallback, useRef } from "react";
import { useInView } from "react-intersection-observer";

export default function Separator() {
  const hrRef = useRef();
  const [inViewRef, inView] = useInView({ triggerOnce: true });

  const setRefs = useCallback(
    (node) => {
      hrRef.current = node;
      inViewRef(node);
    },
    [inViewRef]
  );

  useEffect(() => {
    if (inView) {
      hrRef.current.classList.add("animate");
    }
  }, [inView]);

  return <hr ref={setRefs} />;
}
