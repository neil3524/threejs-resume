/* eslint-disable @next/next/no-img-element */
import SectionTitle from "./SectionTitle";

function EduCard({ date, school, location, img, justifyLeft, justifyRight }) {
  const justifyLeftStyle = justifyLeft ? " justify-left" : justifyRight ? " justify-right" : "";
  const imgElem = <img className="item-media" src={img.url} alt={img.alt || ""} width={250} height={250} />;
  return (
    <div className={"item-container" + justifyLeftStyle}>
      {!justifyRight && img && img.url && imgElem}
      <span>
        <p className="item-date">
          <i>{date}</i>
        </p>
        <p className="item-title">
          <b>{school}</b>
        </p>
        <p>{location}</p>
      </span>
      {justifyRight && img && img.url && imgElem}
    </div>
  );
}
export default function Education() {
  return (
    <section id="Education" className="block">
      <SectionTitle title="Education" description="since 2014" />

      <EduCard
        date={"2019 to 2022"}
        school={
          <>
            DEC <span>(Diplôme D&apos;études Collégiales)</span>: Computer Science
          </>
        }
        justifyLeft
        img={{ url: "images/dawson.jpg", alt: "Dawson" }}
        location={"Dawson College - Montreal, QC"}
      />
      {/* <EduCard
        date={"2014 to 2019"}
        school={<>IB High School Diploma</>}
        justifyRight
        img={{ url: "images/lcchs.png", alt: "LCCHS" }}
        location={"LaSalle Community Comprehensive High School (LCCHS) - LaSalle, QC"}
      /> */}
    </section>
  );
}
