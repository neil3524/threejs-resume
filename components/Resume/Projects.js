/* eslint-disable @next/next/no-img-element */
import { useEffect, useRef, useCallback } from "react";
import { CircularProgress } from "@mui/material";
import { useInView } from "react-intersection-observer";
import SectionTitle from "./SectionTitle";

// import RobotCanvas from "../RobotCanvas/RobotCanvas";

import dynamic from "next/dynamic";
const RobotCanvas = dynamic(() => import("../RobotCanvas/RobotCanvasSuspense"), {
  ssr: false,
});

function Project({ title, description, projectDemo, gitHref }) {
  return (
    <div className="item-container column">
      <div className="project-title-container">
        <h3 className="project-title">{title}</h3>
        {gitHref && (
          <a className="git-btn" href={gitHref}>
            GitLab <img src="images/Git_icon.svg" alt="git" />
          </a>
        )}
      </div>
      <span className="project-description">{description}</span>
      {projectDemo}
    </div>
  );
}

export default function Projects() {
  const [mapInViewRef, mapInView] = useInView({ triggerOnce: true });
  const circularProgressRef = useRef();
  const populationMapRef = useRef();
  const popMapRefs = useCallback(
    (node) => {
      circularProgressRef.current = node;
      mapInViewRef(node);
    },
    [mapInViewRef]
  );

  useEffect(() => {
    // stop map from scrolling
    const map = populationMapRef.current;
    if (map) {
      map.parentElement.addEventListener("click", (e) => {
        map.classList.add("focus");
      });
      map.parentElement.addEventListener("mouseleave", (e) => {
        map.classList.remove("focus");
      });
    }
  }, []);

  const [flockSimInViewRef, flockSimInView] = useInView({ triggerOnce: true });

  return (
    <section id="Projects" className="block">
      <SectionTitle title="Projects" description="School and personal projects" />

      <Project
        title={<>FRC Robotics 2020 3D Interactive Animation</>}
        description={
          <>
            Made using <a href="https://threejs.org/">Three.js</a> wrapped with <a href="https://github.com/pmndrs/react-three-fiber">react-three-fiber</a>
          </>
        }
        projectDemo={
          <div id="canvas-container" className="item-media">
            <RobotCanvas />
          </div>
        }
      />
      <Project
        title={<a href="https://population-map-react-node.herokuapp.com/">Population Map</a>}
        gitHref="https://gitlab.com/dawson-semester-5/web-dev/520-project-neil-kirill"
        description={
          <>
            <p>MERN application that creates a visualization of a worldwide city population dataset.</p>
            <p>Made using MongoDB, Express, React, and Node.js</p>
          </>
        }
        projectDemo={
          <>
            <p style={{ zIndex: 2, fontSize: "0.85em", textAlign: "right", margin: "-1em 1em 0.5em 1em", alignSelf: "flex-end" }}>
              * click then scroll to zoom in/out
            </p>
            <div ref={popMapRefs} id="population-map" className="item-media" style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
              <img src="images/populationmap_placeholder.jpg" alt="population map" />
              <CircularProgress color="inherit" sx={{ zIndex: 1, position: "absolute" }} />
            </div>
            <iframe
              ref={populationMapRef}
              id="population-map"
              className="item-media"
              title="population-map"
              src={mapInView ? "https://population-map-react-node.herokuapp.com/" : ""}
              frameBorder="0"
              style={{ display: "none" }}
              onLoad={
                mapInView
                  ? () => {
                      populationMapRef.current.removeAttribute("style");
                      circularProgressRef.current.remove();
                    }
                  : () => {}
              }
            />
          </>
        }
      />
      <Project
        title={<>Fakijiji (Fake Kijiji)</>}
        gitHref="https://gitlab.com/dawson-semester-4/420-420-team09"
        description={
          <>
            <p>
              Group project - Mock website of the original <a href="https://www.kijiji.ca/">Kijiji</a>
            </p>
            <p>Made using Python and Django</p>
          </>
        }
        projectDemo={<img id="fakijiji" className="item-media" src="images/fakijiji.jpg" alt="fakijiji" width={1918 * 0.5} height={957 * 0.5} />}
      />
      <Project
        title={<>Flocking Simulation</>}
        gitHref="https://gitlab.com/dawson-semester-5/programming/flockingproject_neil_nicolae"
        description={
          <>
            <p>A simulation of the way birds flock in real life.</p>
            <p>Built in C# using MonoGame.</p>
          </>
        }
        projectDemo={
          <div ref={flockSimInViewRef} id="flocking-sim-container" className="item-media">
            <video id="flocking-sim" placeholder="images/flocking_simulation_placeholder.jpg" loop autoPlay muted width={800} height={400}>
              {flockSimInView && <source src="images/flocking_simulation_alpha.webm" type="video/webm" />}
            </video>
          </div>
        }
      />
      <Project
        title={<>Neil&apos;s Portfolio (This Website)</>}
        gitHref="https://gitlab.com/neil3524/threejs-resume"
        description={
          <p>
            Portfolio website built with <a href="https://reactjs.org/">React</a>/<a href="https://nextjs.org/">Next.js</a> with a hint of{" "}
            <a href="https://mui.com/">MUI</a>
          </p>
        }
        projectDemo={<img id="portfolio" className="item-media" src="images/portfolio.jpg" alt="portfolio" width={868} height={597} />}
      />
    </section>
  );
}
