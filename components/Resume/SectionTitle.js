import Separator from "./Separator";

export default function SectionTitle({ title, description }) {
  return (
    <div className="section-title">
      <h2>{title}</h2>
      {description && <p>{description}</p>}
      <Separator />
    </div>
  );
}
