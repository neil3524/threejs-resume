/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useLayoutEffect, useRef } from "react";
import { MathUtils } from "three";

import isTouchScreen from "../IsTouchScreen";

let canvasContainerRef;
let canvasRef;
let introSectionRef;
export default function PaintCanvas({ sectionRef }) {
  if (isTouchScreen()) return null;

  introSectionRef = sectionRef;

  canvasContainerRef = useRef();
  canvasRef = useRef();

  const scrollBarLength = 10; /* parseInt(getComputedStyle(document.body).getPropertyValue("--scrollbar-length")) */

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    const lineWidth = 150;
    const lineCap = "round";
    // const colors = ["#ff8935", "#51e6a5", "#35ffd9", "#35d9ff", "#3535ff"];
    const colors = ["#061e3e", "#251e3e", "#451E3E", "#651E3E", "#801E3E"];
    let colorsIdx = 0;

    const mouse = { x: 0, y: 0 };

    const paint = () => {
      const y = mouse.y - canvas.getBoundingClientRect().top;
      if (y > window.innerHeight) return;
      const x = mouse.x - canvas.getBoundingClientRect().left;
      ctx.beginPath();
      ctx.lineWidth = lineWidth;
      ctx.lineCap = lineCap;
      ctx.strokeStyle = colors[colorsIdx];
      ctx.moveTo(x, y);
      ctx.lineTo(x, y);
      ctx.stroke();
    };

    // paint on canvas whenever the mouse moves
    window.addEventListener("mousemove", (e) => {
      mouse.x = e.clientX;
      mouse.y = e.clientY;
      paint();
    });

    document.querySelector("#intro").addEventListener("mouseenter", () => {
      colorsIdx++;
      if (colorsIdx >= colors.length) colorsIdx = 0;
    });

    const wrapper = document.querySelector("#wrapper");
    const handleScroll = (e) => {
      paint();

      // change shadow so that the "Projects" section is not obstructed by the canvas
      // using THREE.MathUtils...
      const ratio = MathUtils.clamp(wrapper.scrollTop / 800, 0, 1);
      canvasContainerRef.current.style.setProperty("--shadow-spread", `${MathUtils.lerp(80, 460, ratio)}px`);
    };
    wrapper.addEventListener("scroll", handleScroll);
  }, []);

  useLayoutEffect(() => {
    const canvasHeightAndResize = () => {
      const handleResize = () => {
        // canvas dimensions
        const absoluteHeight = getCanvasHeight();
        const absoluteWidth = window.innerWidth - scrollBarLength;
        setCanvasDimensions(absoluteWidth, absoluteHeight);

        // disable canvas if screen is too small
        const clearCanvasBtn = document.querySelector("#clear-canvas");
        if (clearCanvasBtn) {
          if (window.innerWidth <= 600) {
            canvasContainerRef.current.style.display = "none";
            clearCanvasBtn.style.display = "none";
          } else {
            canvasContainerRef.current.style.display = "initial";
            clearCanvasBtn.style.display = "initial";
          }
        }
      };
      window.addEventListener("resize", handleResize);

      handleResize();
    };

    if (sectionRef.current) {
      canvasHeightAndResize();
    }
  });

  return (
    <div ref={canvasContainerRef} id="paint-canvas-container" style={{ width: window.innerWidth, height: getCanvasHeight() }}>
      <canvas ref={canvasRef} id="paint-canvas" width={window.innerWidth} height={getCanvasHeight()} />
    </div>
  );
}

function setCanvasDimensions(width, height) {
  // wrapper
  // const wrapper = document.querySelector("#paint-canvas-container");
  const container = canvasContainerRef.current;
  container.style.width = `${width}px`;
  container.style.height = `${height}px`;
  // canvas
  // const canvas = document.querySelector("#paint-canvas");
  const canvas = canvasRef.current;
  canvas.width = width;
  canvas.height = height;
}

function getCanvasHeight() {
  // return sectionRef.current ? sectionRef.current.offsetHeight + parseInt(getComputedStyle(sectionRef.current).marginBottom) : window.innerHeight + 96;
  return window.innerHeight + 96;
}
