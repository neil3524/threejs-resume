/* eslint-disable @next/next/no-img-element */
import Separator from "./Separator";

export default function Awards() {
  return (
    <section id="Awards" className="block">
      <div className="section-title">
        <h2>Awards</h2>
        <p>Awarded for extraordinary determination and leadership in Robotics</p>
        <Separator />
      </div>
      <div className="item-container justify-left">
        <img className="left-image item-media" src="images/silver_wolf.jpg" alt="Silver Wolf Trophy" width={4032 * 0.08} height={3024 * 0.08} />
        <span className="award-item">
          <p className="item-date">
            <i>2019</i>
          </p>
          <p className="item-title">
            <b>LCCHS Silver Wolf Trophy</b>
          </p>
          <p>
            <u>Don Robichaud Memorial Award for Excellence in Arts and Science</u>
          </p>
        </span>
      </div>
      <div className="item-container justify-right">
        <span className="award-item right-text">
          <p className="item-date">
            <i>2019</i>
          </p>
          <p className="item-title">
            <b>Youth Fusion Leaders MTL Scholarship</b>
          </p>
          <p>
            <u>College Scholarship</u>
          </p>
        </span>
        <img className="right-image item-media" src="images/Youth_Fusion_logo2.jpg" alt="Youth Fusion logo" width={250} height={250} />
      </div>
    </section>
  );
}
