/* eslint-disable @next/next/no-img-element */
import { useRef, useEffect } from "react";

import dynamic from "next/dynamic";
const PaintCanvas = dynamic(() => import("./IntroPaintCanvas"), {
  ssr: false,
});

export default function Intro() {
  const myTitleRef = useRef();

  // const animateTitleCss = (animationClasses) => {
  //   // css animation
  //   let animationIndex = 0;
  //   const handleAnimationEnd = () => {
  //     animationIndex++;
  //     if (animationIndex >= animationClasses.keys.length) animationIndex = 0;
  //     const animationClassSelected = animationClasses.keys[animationIndex];
  //     myTitleRef.current.textContent = animationClassSelected;
  //     myTitleRef.current.style.setProperty("--color", animationClasses[animationClassSelected].color);
  //     myTitleRef.current.className = animationClasses[animationClassSelected].class;
  //   };
  //   myTitleRef.current.addEventListener("animationend", handleAnimationEnd);
  //   handleAnimationEnd();
  // };

  let typeInterval;
  let backspaceInterval;
  let delayTypeTimeout;
  let delayBackspaceTimeout;
  const animateTitleJS = (animations) => {
    const animationTime = 2000;
    const animationTimeBackspace = 1000;
    const animationDelayStart = 500;
    const animationDelayEnd = 750;
    const animation = (animationIndex = 0) => {
      if (animationIndex >= animations.keys.length) animationIndex = 0;
      const animationKey = animations.keys[animationIndex];
      let titleLength = 0;
      const type = () => {
        if (titleLength === animationKey.length) {
          myTitleRef.current.style.setProperty("--animation-time", "1060ms");
          if (!delayTypeTimeout) {
            delayTypeTimeout = setTimeout(() => {
              clearInterval(typeInterval);
              typeInterval = undefined;
              delayTypeTimeout = undefined;
              backspaceInterval = setInterval(() => backspace(), animationTimeBackspace / animationKey.length);
            }, animationDelayEnd);
          }
        } else {
          titleLength++;
          myTitleRef.current.style.setProperty("--animation-time", "0");
          myTitleRef.current.style.setProperty("--color", animations[animationKey].color);
          const substring = animationKey.substring(0, titleLength).replace(/\s/g, "&nbsp;");
          myTitleRef.current.innerHTML = substring;
        }
      };
      const backspace = () => {
        if (titleLength === 0) {
          myTitleRef.current.style.setProperty("--animation-time", "1060ms");
          if (!delayBackspaceTimeout) {
            delayBackspaceTimeout = setTimeout(() => {
              clearInterval(backspaceInterval);
              backspaceInterval = undefined;
              delayBackspaceTimeout = undefined;
              // restart animation
              animation(animationIndex + 1);
            }, animationDelayStart);
          }
        } else {
          titleLength--;
          myTitleRef.current.style.setProperty("--animation-time", "0");
          myTitleRef.current.style.setProperty("--color", animations[animationKey].color);
          const substring = animationKey.substring(0, titleLength).replace(/\s/g, "&nbsp;");
          myTitleRef.current.innerHTML = substring;
        }
      };
      typeInterval = setInterval(() => type(), animationTime / animationKey.length);
    };
    animation();
  };

  useEffect(() => {
    // table to relate titles to classes
    const animations = {
      "CompSci Student": { class: "student", color: "#51e6a5" },
      "Robotics Enthusiast": { class: "robotics", color: "#009bde" },
    };
    animations.keys = Object.keys(animations);
    // animateTitleCss(animationClasses);
    animateTitleJS(animations);

    return () => {
      clearInterval(typeInterval);
      clearInterval(backspaceInterval);
      clearTimeout(delayTypeTimeout);
      clearTimeout(delayBackspaceTimeout);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const sectionRef = useRef();

  return (
    <>
      <section ref={sectionRef} id="intro" className="block">
        <div style={{ zIndex: "inherit", pointerEvents: "none" }}>
          <h2 id="im">I&apos;m</h2>
          <h1 id="my-name">Neil Fisher</h1>
          <p ref={myTitleRef} id="my-title"></p>
          <div id="languages">
            <p id="JS">JavaScript</p>
            <p id="React">React</p>
            <p id="CS">
              <b>C</b>#
            </p>
            <p id="Java">Java</p>
            <p id="Kotlin">Kotlin</p>
            <p id="Python">Python</p>
            <p id="Cpp">C++</p>
            <p id="SQL">SQL</p>
            <p id="Git">Git</p>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img id="me-img" src="images/me.jpg" alt="Neil" style={{ zIndex: "inherit" }} width={200} height={200} />
          <p id="email">
            Email: <a href="mailto:neil@neilf.dev">neil@neilf.dev</a>
          </p>
        </div>
      </section>
      <PaintCanvas sectionRef={sectionRef} />
    </>
  );
}
