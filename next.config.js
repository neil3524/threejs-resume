let basePath = process.env.BASE_PATH;
if (basePath === "" || basePath === undefined) {
  basePath = "https://neil3524.gitlab.io/threejs-resume";
}

const assetPrefix = process.env.NODE_ENV === "production" ? basePath : "";
console.log("NEXT.CONFIG.JS assetPrefix:", assetPrefix);
console.log("process.env.NODE_ENV", process.env.NODE_ENV);

module.exports = {
  reactStrictMode: true,
  assetPrefix: assetPrefix,
};
