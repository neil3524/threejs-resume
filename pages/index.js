import Head from "next/head";
import App from "../components/App";

export default function Index() {
  return (
    <>
      <Head>
        <link rel="icon" href={"./sonichowl-white.svg"} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="A 3d resume built using React and Three.js." />
        <link rel="manifest" href={"./manifest.json"} />
        <title>Neil&apos;s Portfolio</title>
      </Head>
      <App />
    </>
  );
}
